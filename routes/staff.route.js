const express         = require('express');
const router          = express.Router();
const staffRoutes     = require('../controllers/staff.controller');

router.get('/', staffRoutes.getAllStaffs);
router.get('/:id',  staffRoutes.getStaffByID);
router.post('/',  staffRoutes.createStaff);
router.put('/:id',  staffRoutes.updateStaff);
router.delete('/:id',  staffRoutes.deleteStaff);

module.exports = router;