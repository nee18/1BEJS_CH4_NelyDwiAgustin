const express           = require('express');
const router            = express.Router();
const { authenticate }  = require('../middleware/auth')
const staffRoutes       = require('./staff.route');
const accountRoutes     = require('./account.route');
const roleRoutes        = require('./role.route');

router.use('/staff', staffRoutes);
router.use('/account', accountRoutes);
router.use('/role', roleRoutes);

module.exports = router;