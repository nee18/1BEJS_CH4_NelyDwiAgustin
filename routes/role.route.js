const express        = require('express');
const router         = express.Router();
const roleRoutes     = require('../controllers/role.controller');

router.get('/', roleRoutes.getAllRoles);
router.get('/:id',  roleRoutes.getAccountByID);
router.post('/',  roleRoutes.createdAccount);
router.put('/:id',  roleRoutes.updateAccount);
router.delete('/:id',  roleRoutes.deleteAccount);

module.exports = router;