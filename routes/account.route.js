const express           = require('express');
const router            = express.Router();
const accountRoutes     = require('../controllers/account.controller');

router.get('/', accountRoutes.getAllAccount);
router.get('/:id',  accountRoutes.getAccountByID);
router.post('/',  accountRoutes.createAccount);
router.put('/:id',  accountRoutes.updateAccount);
router.delete('/:id',  accountRoutes.deleteAccount);

module.exports = router;