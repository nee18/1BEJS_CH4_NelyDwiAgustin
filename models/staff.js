'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Staff extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Staff.hasOne(models.Account, {
        foreignKey: 'staff_id'
      });
      Staff.belongsTo(models.Role, {
        foreignKey: 'role_id'
      });
    }
  }
  Staff.init({
    name: DataTypes.STRING,
    city: DataTypes.STRING,
    role_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Staff',
  });
  return Staff;
};