'use strict';
const staffsData = require('../masterdata/staffs.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const dataStaffsToBeSeeded = staffsData.map((eachStaffData) => {
      return {
        name: eachStaffData.name,
        city: eachStaffData.city,
        role_id: eachStaffData.role_id,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    })
    await queryInterface.bulkInsert('Staffs', dataStaffsToBeSeeded, {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Staffs');
  }
};
